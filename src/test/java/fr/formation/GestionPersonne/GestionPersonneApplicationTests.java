package fr.formation.GestionPersonne;

import fr.formation.GestionPersonne.bll.ContactManager;
import fr.formation.GestionPersonne.bll.ContactManagerImpl;
import fr.formation.GestionPersonne.bo.Contact;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GestionPersonneApplicationTests {

	@Autowired
	ContactManager contactManager;
	@Test
	public void contextLoads() {
		Contact c1=new Contact("lina","drissi","0246985558");
		Contact c2=new Contact("Dupont","drissi","0246985558");
		contactManager.addContact(c1);
		contactManager.addContact(c2);
		contactManager.getAllContact();
	}

}

