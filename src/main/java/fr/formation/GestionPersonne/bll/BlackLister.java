package fr.formation.GestionPersonne.bll;

import fr.formation.GestionPersonne.bo.Contact;
import org.springframework.stereotype.Component;

@Component
public class BlackLister {
    public Boolean tester(Contact contact) {
        boolean a = true;
        if (("dupont".equals(contact.getNom().toLowerCase())) || ("durant".equals(contact.getNom().toLowerCase()))) {
            a = false;
        }
        return a;
    }
}
