package fr.formation.GestionPersonne.bll;

import fr.formation.GestionPersonne.bo.Contact;

import java.util.List;

public interface ContactManager {
    public void addContact(Contact contact);
    public List<Contact> getAllContact();
}
