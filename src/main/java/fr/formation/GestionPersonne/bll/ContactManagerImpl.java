package fr.formation.GestionPersonne.bll;

import fr.formation.GestionPersonne.bo.Contact;
import fr.formation.GestionPersonne.dal.ContactDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ContactManagerImpl implements ContactManager {

    @Autowired
    BlackLister blackLister;
    @Autowired
    ContactDao dao;
    @Override
    public void addContact(Contact contact) {
        if(blackLister.tester(contact)) {
            dao.insert(contact);
        }
        else{
            System.out.println("tu peut pas inseret cette contact!!!");
        }
    }

    @Override
    public List<Contact> getAllContact() {
        List<Contact> lst= dao.getAll();
        return lst;
    }
}
