package fr.formation.GestionPersonne.dal;

import fr.formation.GestionPersonne.bo.Contact;

import java.util.List;

public interface ContactDao {
    public void insert (Contact contat);
    public List<Contact> getAll();
}
